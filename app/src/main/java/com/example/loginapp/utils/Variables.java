package com.example.loginapp.utils;

import org.json.JSONObject;

public class Variables {
    public static CharSequence headerTitle = "";

    public static JSONObject qcStore;
    //public static JSONObject dcStore;

    public static String source;

    public static int attemptedApproves = 0;

    public static final String ADAPTER_PACKAGE = "com.example.loginapp.adapter";
    public static final String ASYNCHRONOUS_PACKAGE = "com.example.loginapp.asynchronousclasses";
    public static final String MODEL_PACKAGE = "com.example.loginapp.com.example.loginapp.model";
    public static final String MOBILECSA_PACKAGE = "com.example.loginapp";

    public static int currentPage = -1;
    public static int totalCount = 0;
    public static int lastPage = 0;

    public static String dcRawResult = "";
}
