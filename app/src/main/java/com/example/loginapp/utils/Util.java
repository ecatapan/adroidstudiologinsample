package com.example.loginapp.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.example.loginapp.R;

public class Util {

    public static SharedPreferences sharedPreferences;

    public static int systemHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
    public static int systemWidth = Resources.getSystem().getDisplayMetrics().widthPixels;

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static final int CONNECTION_TIMEOUT = 25_000;
    public static final int READ_TIMEOUT = 25_000;

    public static int recyclerViewItemHeight = 0;

    public static boolean validUserPass(String pass) {
        return (pass.length() > 7 && pass.length() < 33);
    }

    public static void alertBox(final Context context, String msg) {
        AlertDialog.Builder warningBox = new AlertDialog.Builder(context);

        warningBox.setMessage(msg);

        warningBox.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });
        warningBox.create().show();
    }

    public static Dialog getProgressBar(Context context) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        if(dialog.getWindow() != null ) {
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                   WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN );
        } else {
            Util.longToast(context, "Dialog Window is null");
        }
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.progressbar_layout);
        dialog.setCancelable(true);
        return dialog;
    }

    public static void shortToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void longToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }



    public static void alertBox(final Context context, String message, String title, final boolean minimizable) {
        AlertDialog.Builder warningBox = new AlertDialog.Builder(context);

        warningBox.setTitle(title);
        warningBox.setMessage(message);
        warningBox.setCancelable(false);

        warningBox.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

                if (minimizable){
                    Intent startMain = new Intent(Intent.ACTION_MAIN);
                    startMain.addCategory(Intent.CATEGORY_HOME);
                    startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    context.startActivity(startMain);
                }
            }

        });

    }
    public static void displayStackTraceArray(StackTraceElement[] stackTraceElements,
          String packageRoot, String exceptionName, String toString) {
        Log.e(exceptionName, toString);
        for (StackTraceElement elem : stackTraceElements) {
            if (elem.toString().contains(packageRoot)) {
                Log.e("src", elem.toString());
            }
        }
    }
}
