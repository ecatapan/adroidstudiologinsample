package com.example.loginapp;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.loginapp.adapter.SettingsAdapter;
import com.example.loginapp.model.KeyValueInfo;

import java.util.ArrayList;

public class SettingsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = findViewById(R.id.toolbarSettings);
        toolbar.setTitle("Settings");
        setSupportActionBar(toolbar);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(
                SettingsActivity.this);

        ArrayList<KeyValueInfo> settingsModelList = new ArrayList<>();
        RecyclerView recyclerView = findViewById(R.id.rvSettingsList);

        String northSim = preferences.getString("northSim", null);
        String centralSim = preferences.getString("centralSim", null);
        String southSim = preferences.getString("southSim", null);
        String dumagueteSim = preferences.getString("dumagueteSim", null);


        String northWifi = preferences.getString("northWifi", null);
        String centralWifi = preferences.getString("centralWifi", null);
        String southWifi = preferences.getString("southWifi", null);
        String dumagueteWifi = preferences.getString("dumagueteWifi", null);

        String mcsaPasswordPrefs = preferences.getString("mcsaPasswordPrefs", null);
        String domain = preferences.getString("domain", null);

        settingsModelList.add(new KeyValueInfo("North SIM", northSim == null ? "" : northSim));
        settingsModelList.add(new KeyValueInfo("North Wifi", northWifi == null ? "" : northWifi));

        settingsModelList.add(new KeyValueInfo("Central SIM", centralSim == null ? "" : centralSim));
        settingsModelList.add(new KeyValueInfo("Central Wifi", centralWifi == null ? "" : centralWifi));

        settingsModelList.add(new KeyValueInfo("South SIM", southSim == null ? "" : southSim));
        settingsModelList.add(new KeyValueInfo("South Wifi", southWifi == null ? "" : southWifi));

        settingsModelList.add(new KeyValueInfo("Dumaguete SIM", dumagueteSim == null ? "" : dumagueteSim));
        settingsModelList.add(new KeyValueInfo("Dumaguete Wifi", dumagueteWifi == null ? "" : dumagueteWifi));

        settingsModelList.add(new KeyValueInfo("Domain", domain == null ? "" : domain));
        settingsModelList.add(new KeyValueInfo("Password", mcsaPasswordPrefs == null ? "" : mcsaPasswordPrefs));

        settingsModelList.add(new KeyValueInfo("", ""));

        SettingsAdapter settingsAdapter = new SettingsAdapter(settingsModelList,
                SettingsActivity.this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(SettingsActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(settingsAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(SettingsActivity.this,
                LinearLayoutManager.VERTICAL));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
