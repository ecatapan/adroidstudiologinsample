package com.example.loginapp;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentTransaction;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.lang.ref.WeakReference;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class LoginActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);

        if (sharedPreferences.getString("user", null) == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content_login, new LoginFragment());
            fragmentTransaction.commit();

        } else {
            new SessionExpiryTask(this).execute(
                    sharedPreferences.getString("user", null), sharedPreferences.getString("sessionId", null),
                    String.valueOf(sharedPreferences.getInt("csaId", 0)));

        }

    }

    private static class SessionExpiryTask extends AsyncTask<String, String, String> {
        private final WeakReference<LoginActivity> activityWeakReference;

        private HttpURLConnection conn = null;

        private final SharedPreferences sharedPreferences;

        long startTime;

        private SessionExpiryTask(LoginActivity context) {
            activityWeakReference = new WeakReference<>(context);
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            startTime = System.nanoTime();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String[] args) {
            try {
                URL url = new URL(sharedPreferences.getString("domain", null) + "checksessionexpiry");
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(5000);
                conn.setConnectTimeout(5000);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept", "*/*");
                conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
                conn.setRequestProperty("Connection", "keep-alive");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; " +
                        "charset=utf-8");
                conn.setRequestProperty("Cookie", "JSESSIONID=" +
                        sharedPreferences.getString("sessionId", null));

                conn.setDoInput(true);
                conn.setDoOutput(true);

                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("sessionId", args[0])
                        .appendQueryParameter("csaId", args[1]);

                String query = builder.build().getEncodedQuery();
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8));

                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

                int statusCode = conn.getResponseCode();
                if (statusCode == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder stringBuilder = new StringBuilder();

                    String line;
                    while ((line = reader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    input.close();
                    reader.close();

                    if (stringBuilder.toString().isEmpty()) {
                        return "{\"success\": false, \"reason\": \"No response from server.\"}";

                    } else {
                        return stringBuilder.toString();
                    }


                } else {
                    return "{\"success\": false, \"reason\": \"Request did not succeed. " +
                            "Status Code: " + statusCode + "\"}";
                }
            } catch (MalformedURLException | ConnectException | SocketTimeoutException e) {
                if (e instanceof MalformedURLException) {
                    return "{\"success\": false, \"reason\": \"Malformed URL.\"}";

                } else if (e instanceof ConnectException) {
                    return "{\"success\": false, \"reason\": \"Cannot Connect to server. " + "Check wifi or mobile data and check if the server is available.\"}";
                } else {
                    return "{\"success\": false, \"reason\": \"Connection timed out. " +
                            "The server is taking to long to reply.\"}";
                }
            } catch (Exception e) {
                return "{\"success\": false, \"reason\": \"" + e.getMessage() + "\"}";
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }
        }

        @Override
        protected void onPostExecute(String result) {
            LoginActivity loginActivity = activityWeakReference.get();

            if (loginActivity == null || loginActivity.isFinishing()) {
                return;
            }
            try {
                JSONObject responseJson = new JSONObject(result);
                if (responseJson.getBoolean("success")) {
                    Intent intent = new Intent(loginActivity, MainActivity.class);
                    loginActivity.startActivity(intent);
                    loginActivity.finish();
                } else {
                    FragmentTransaction fragmentTransaction = loginActivity
                            .getSupportFragmentManager().beginTransaction();

                    fragmentTransaction.replace(R.id.content_login, new LoginFragment());
                    fragmentTransaction.commit();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
